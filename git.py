#!/bin/python3

# THIS SCRIPT MAY DESTROY YOUR SYSTEM 
# USE AT YOUR OWN RISK
# DON'T BLAME ME LATER FOR DAMAGES!!
# YOU HAVE BEEN WARNED!!!!!!!!!!!!!!!

gitFolderPath = "/home/me/git"
gitOriginalPath = "/bin/git"
defaultFolderPermissions = 0o755;
import os
import sys
    # This script creates a folder with a username of the repo's owner
    # then downloads repo in that folder to keep everything clean and tidy
    
    # KuchiKuu
    # 2021-03-08
    
    # My usage:
    #       >Script location ~/.local/bin/git
    #       >Adding this location to PATH as first to be evaluated before original
    #           /bin that contains normal git command
    #       >.bashrc: PATH=/home/me/.local/bin:$PATH
    #       >I have a master "git" folder in my home where I download all git repos
    #           That's why I'm using gitFolderPath as /home/me/git where I want this
    #           script to be executed
    #       >Only do this if the command is clone and if the script is executed in
    #           the gitFolderPath. If the conditions are not met, hand commands
    #           to the normal git, located at gitOriginalPath
    
    # Basic URL:
    #     https://github.com/KrispyCamel4u/SysMonTask
    #       [0]   [1]          [2]                          [3]                   [4]
    # Do this only if executed in core git folder
    # Change script's name to normal git path
sys.argv[0] = gitOriginalPath
if(sys.argv[1] == "clone"):
   if(os.getcwd() == gitFolderPath):
        getFolder = sys.argv[2].split("/")[3]
        if(os.path.isfile(getFolder)):
            print(f'A file named {getFolder} already exists.\nCan not create a folder.\nExiting...')
            exit()
        if(os.path.isdir(getFolder)):
            os.chdir(getFolder)
        else:
            os.mkdir(getFolder,defaultFolderPermissions)
            os.chdir(getFolder)
        #all good, proceed with the normal git command
# if conditions are not met, proceed with the normal git command
os.system(" ".join(sys.argv))
